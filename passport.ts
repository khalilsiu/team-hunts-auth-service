import * as passport from 'passport'
import jwt from './jwt'
import * as passportJWT from 'passport-jwt'
import Admin from './models/admin'

const JWTStrategy = passportJWT.Strategy
const { ExtractJwt } = passportJWT

  passport.use(
    new JWTStrategy(
      {
        secretOrKey: jwt.jwtSecret,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      },
      async (payload, done) => {
        console.log(payload);
        const user: any = await Admin.findOne({ email: payload.email })
        if (!user) done('User not found', null)
        else {
          const payload = {
            email: user.email,
            id: user._id,
            permission: user.permission
          }
          done(null, payload);
        }
      }
    )
  )


 
