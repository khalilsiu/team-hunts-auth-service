import Admin from '../models/admin'
import { checkPassword } from '../hash'
import * as jwtSimple from 'jwt-simple'
import jwt from '../jwt'

export const login = async (email: string, password: string) => {
  try {
    const user: any = await Admin.findOne({ email: email })
    if (!user) throw new Error('Username/Password Error')
    const match = await checkPassword(password, user.hash)
    if (!match) throw new Error('Username/Password Error')
    const payload = {
      id: user._id,
      email: user.email,
    }
    const token = jwtSimple.encode(payload, jwt.jwtSecret)
    return token
  } catch (e) {
    throw new Error(e.toString())
  }
}
