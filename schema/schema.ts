import * as graphql from 'graphql'
import * as mongoose from 'mongoose'
import * as _ from 'lodash'
import Admin from '../models/admin'
import Game from '../models/game'
// import { login } from './Auth'

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLFloat,
  GraphQLBoolean,
  GraphQLList,
} = graphql

mongoose.connect(
  'mongodb+srv://anthony:dbs123@cluster0-csy6z.mongodb.net/test?retryWrites=true&w=majority',
  {
    useUnifiedTopology: true,
  }
)

mongoose.connection.once('open', () => {
  console.log('connected to database')
})

const AdminType = new GraphQLObjectType({
  name: 'Admin',
  //need to be in a function so that this can help with reference issues
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    email: { type: GraphQLString },
    hash: { type: GraphQLString },
    phone: { type: GraphQLString },
    token: { type: GraphQLString },
    games: {
      type: new GraphQLList(GameType),
      resolve: (parent, args) => {
        // return _(dummyGame)
        // .keyBy('id') // or .indexBy() if using lodash 3.x
        // .at(parent.gameId)
        // .value();
      },
    },
  }),
})

const LocationType = new GraphQLObjectType({
  name: 'Location',
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    long: { type: GraphQLFloat },
    lat: { type: GraphQLFloat },
  }),
})

const GameType = new GraphQLObjectType({
  name: 'Game',
  //need to be in a function so that this can help with reference issues
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    duration: { type: GraphQLFloat },
    startLocation: { type: LocationType },
    started: { type: GraphQLBoolean },
    startTime: { type: GraphQLInt },
  }),
})

// RootQuery is the place where the client app starts their query, the entry point
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    admin: {
      type: AdminType,
      // we expect the admin query to pass in an id
      args: { id: { type: GraphQLID } },
      resolve: (parent, args) => {
        // codes to get data from the db
        // we have access to args.id to grab data
        // type of id is treated as string, GraphQLID is for the use of graphql
        // return _.find(dummyData, {id: args.id})
      },
    },
    admins: {
      type: new GraphQLList(AdminType),
      resolve(parent, args) {
        // return dummyData
      },
    },
    game: {
      type: GameType,
      args: { id: { type: GraphQLID } },
      resolve: (parent, args) => {
        // return _.find(dummyGame, {id: args.id})
      },
    },
  },
})

// we need to explicitly specify mutation to make changes to data
const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    loginAdmin: {
      type: AdminType,
      args: {
        email: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      // resolve: async (parent, args) => await login(args)
    },
    addAdmin: {
      type: AdminType,
      args: {
        email: { type: GraphQLString },
        hash: { type: GraphQLString },
      },
      resolve: (parent, args) => {
        // mongoose
        const admin = new Admin({
          email: args.email,
          hash: args.hash,
        })
        // save to mongoose
        return admin.save()
      },
    },
    addGame: {
      type: GameType,
      args: {
        name: { type: GraphQLString },
        duration: { type: GraphQLFloat },
      },
      resolve: (parent, args) => {
        const game = new Game({
          name: args.name,
          duration: args.duration,
        })
        return game.save()
      },
    },
  },
})

// export the schema to be used in graphqlHTTP
export default new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
})
