import { Model, Document } from 'mongoose'
import { checkPassword } from '../hash'
import * as jwtSimple from 'jwt-simple'
import jwt from '../jwt'
import {hashPassword} from '../hash'

export default class AuthService {
  private Admin: Model<Document, {}>
  constructor(adminModel: Model<Document, {}>) {
    this.Admin = adminModel
  }

  public login = async (email: string, password: string) => {
    const user: any = await this.Admin.findOne({ email })
    if (!user) throw new Error('Email/Password Error')
    const match = await checkPassword(password, user.hash)
    if (!match) throw new Error('Email/Password Error')
    const payload = {
      id: user._id,
      email: user.email,
    }
    const token = jwtSimple.encode(payload, jwt.jwtSecret)
    return token
  }

  public getUser = async (email: string) => {
    const match = await this.Admin.findOne({email});
    if (!match) throw new Error("Email does not exist.")
    // password validation to be done
    return match
  }

  public createUser = async (email: string, password: string) => {
    const match = await this.Admin.findOne({email});
    if (match) throw new Error("Email already exists.")
    // password validation to be done
    const hash = await hashPassword(password);
    const newAdmin = new this.Admin({ email, hash })
    return await newAdmin.save()
  }

  public updateUser = async (email: string, password: string, phone: string, name: string, permission: string[]) => {
    let match: any = await this.Admin.findOne({email});
    if (!match) throw new Error("Email does not exist.")
    // password validation to be done
    if (password){
      match.hash = await hashPassword(password);
    }
    if (phone) match.phone = phone;
    if (name) match.name = name;
    if (permission && permission.length > 0) match.permission  = permission;
    return await match.save()
  }
}
