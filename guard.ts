import * as passport from 'passport'

// you would need to add successRedirect and failureRedirect to it
export const isLoggedIn = passport.authenticate('jwt', {session:false})