import * as express from 'express'
import { Request, Response, NextFunction } from 'express'
import AuthService from '../services/AuthService'

export default class AuthRouter {
  private authService: AuthService
  constructor(authService: AuthService) {
    this.authService = authService
  }

  public router() {
    const router = express.Router()
    router.post('/create', this.createUser)
    router.put('/update', this.updateUser)
    router.get('/', this.getUser)
    return router
  }

  private getUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { email } = req.body
        if (req.user){
            const {permission} = req.user as any;
            if (permission.includes('user.get') || permission.includes('user.*'))
            res.json(await this.authService.getUser(email))
            else res.status(403).json({isSuccess: false, msg: 'Create user is not permitted.'})
        }
    }catch(e){
        next(e);
    }
  }

  private createUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { email, password } = req.body
        if (req.user){
            const {permission} = req.user as any;
            if (permission.includes('user.create') || permission.includes('user.*'))
                res.json(await this.authService.createUser(email, password))
            else res.status(403).json({isSuccess: false, msg: 'Create user is not permitted.'})
        }
    }catch(e){
        next(e);
    }
  }

  private updateUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { email, password, phone, name, permission } = req.body
        if (req.user){
            const userPermission = (req.user as any).permission
            if (userPermission.includes('user.update') || userPermission.includes('user.*'))
            res.json(await this.authService.updateUser(email, password, phone, name, permission))
            else res.status(403).json({isSuccess: false, msg: 'Update user is not permitted.'})
        }
        
    }catch(e){
        next(e);
    }
  }
}
